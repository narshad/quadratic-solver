import math
import socket
from flask import render_template, request, jsonify
from flask import Flask
import sys


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/solve_quadratic', methods=['POST'])
def solve_quadratic():
    return_data = {'status': 'OK', 'ip': 'host-ip: '+socket.gethostbyname(socket.gethostname())}
    try:
        input_a = float(request.form['input-a'])
        input_b = float(request.form['input-b'])
        input_c = float(request.form['input-c'])
    except:
        return_data['result'] = 'Wrong Input'
        return jsonify(return_data)
    result = get_quadratic_solution(input_a, input_b, input_c)
    return_data['result'] =  result
    return jsonify(return_data)

def get_quadratic_solution(a, b, c):
    # discriminant
    d = b**2-4*a*c

    try:
        if d < 0:
            return "This equation has no real solution"
        elif d == 0:
            x = (-b+math.sqrt(b**2-4*a*c))/2*a
            return "This equation has one solutions: %f" % x
        else:
            x1 = (-b+math.sqrt((b**2)-(4*(a*c))))/(2*a)
            x2 = (-b-math.sqrt((b**2)-(4*(a*c))))/(2*a)
            return 'This equation has two solutions: %f or %f' % (x1, x2)
    except: # catch *all* exceptions
        return 'Error: %s' % sys.exc_info()[0]

if __name__ == '__main__':
    app.run(host='0.0.0.0')
