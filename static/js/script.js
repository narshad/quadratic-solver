/**
 * Created by naeem.arshad on 3/27/2016.
 */
$(function () {
    $('#submit').click(function () {
        $.ajax({
            url: '/solve_quadratic',
            data: $('form').serialize(),
            type: 'POST',
            success: function (response) {
                console.log(response);
                 $("#ip").text(response.ip);
                 $("#result").text(response.result);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $('#btn-clear').click(function(){
        $(".form-control").val('');
        $(".result").text('');
    });
});
